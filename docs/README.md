---
home: true
heroImage: images/me.jpg
heroText: Christopher Karl-Heinz Zeuch
tagline: Curriculum Vitae
footer: COPYRIGHT © Christopher Zeuch 2020
---

# About Me

I am a hardworking, self-motivated and highly autonomous individual with a passion for assuring quality on all levels and aspects within the software development cycle.

I have experience in integrating into software development teams and finding my feet fairly quickly.
I work well within and alongside teams.

I'm not afraid to jump in and get my hands dirty or ask stupid questions.
I have a love for enabling efficiency, creating documentation as well as visibility for stakeholders.

I have designed and executed manual and automated test plans for a variety of fintech-based products;
as well as coordinating large scale regressions and communicating the health and status to all relevant stakeholders on a daily basis during those periods.

I love learning new things as well as mentoring others.
I care a lot about the people around me and do what I can to make their lives easier within the SDLC.

I hope that this as well as my [work experience][1] and [education][2] piques your interest in me.

<div class="landing">
  <div class="features">
    <div class="feature">
      <h3>Testing Experience</h3>
      <p>
        - Mobile application testing<br>
        - Backend application testing<br>
        - Test plan creation and execution<br>
        - Release planning and co-ordination<br>
        - Testing environment setup & maintenance<br>
        - Test suite maintenance<br>
      </p>
    </div>
    <div class="feature">
      <h3>Tools</h3>
      <p>
        - AWS<br>
        - Argo<br>
        - BitBucket<br>
        - Confluence<br>
        - GitLab<br>
        - Insomnia<br>
        - Jenkins<br>
        - Jira<br>
        - Portus<br>
        - SoapUI<br>
        - Zephyr<br>
      </p>
    </div>
    <div class="feature">
      <h3>Skills, Stacks & Frameworks</h3>
      <p>
        - Automated testing<br>
        - Docker<br>
        - Gherkin<br>
        - Git<br>
        - Kubernetes<br>
        - Manual testing<br>
        - Markdown<br>
        - JSON<br>
        - REST<br>
        - SOAP<br>
        - Terraform<br>
        - VuePress<br>
        - XML<br>
        - YAML<br>
      </p>
    </div>
  </div>
</div>

[1]: /experience/
[2]: /education/
