# Cover Letter

To who it may concern

I am interesting in applying for the Gaming QA Lead position as advertised on your website.

I am a Senior QE Analyst in the Fintech industry, with experience in testing, improving and releasing software for almost five year.
I've worked in and alongside Frontend teams, Backend teams, Automation team and more recently, our DevOps team.
I've also been an unofficial QA lead to any QA contractors that were hired, onboarding them myself and ensuring they had everything they needed to succeed.
Currently, I am responsible for coordinating and driving quality best practices across 6 teams and ensuring that the DevOps team can work efficiently while supporting Engineering.

I have an absolute passion for quality, documentation and enabling teams to be as efficient as possible.
I am an extremely organized and autonomous individual, constantly ensuring any gaps in our SDLC and processes are visible and improved.

QA is more than just a hurdle or a checklist within a development cycle.
Quality Assurance plays an important role in ensuring that the amazing products and features being developed function as is intended, on a consistent basis.
It is important that developers get the feedback they need in a clear and concise manner.



I've been an avid gamer since the age of five and my love and passion for games has only grown since then.
In my spare time I've written game reviews and tinkering with tools and technology.

I hope that my cover letter has piqued your interest in me and I look forward to hearing from you.

Kind Regards
