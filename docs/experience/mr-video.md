# MR. Video

<colorBadge text="June 2011 – August 2012" type="green"/>

## Company Background

[Mr. Video][1] is a South African DVD rental store.

## Experience

### Cashier

<colorBadge text="June 2011 – March 2012" type="blue"/>

As a Cashier I was responsible for operating the POS system and cashing up at the end of the day.

My duties also included:

- Ensuring the store was clean at all times
- Completing a weekly stock take
- Checking in all the DVD's from the previous day
- Cleaning the DVD's if necessary
- Following up on late returns

### Manager

<colorBadge text="April 2012 – August 2012" type="blue"/>

My promotion to Store Manager meant that my duties now included:

- Opening and closing the store
- Ensuring that we advertise the new releases before they arrive

At this time the franchise owners moved from Somerset West back to Port Elizabeth.
This meant that I was the direct point of contact for any customer concerns.

I worked with two Sales Assistants and ensured that they opened and closed the store on time and that they cashed up correctly.

[1]: https://www.mrvideo.co.za/
