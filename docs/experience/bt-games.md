# BT Games

<colorBadge text="December 2012 – May 2015" type="green"/>

## Company Background

[BT Games][1] is South Africa's largest gaming specialist store.

## Experience

### Sales Representative

<colorBadge text="December 2012 – March 2013" type="blue"/>

As a Sales Representative I was responsible for operating the POS system and assisting customers.

My duties also included:

- Ensuring the display racks are organized
- Completing stock take with the rest of the team
- Assisting customers with returns and refunds

### Acting Manager

<colorBadge text="April 2013 – May 2015" type="blue"/>

As the Acting Manager I was responsible for running the store on the days when I did not work alongside my Manager.  
My promotion to Acting Manager meant that my duties now included:

- Completing a daily stock take
- Completing a weekly stock take
- Receiving stock and packing the store room in an organized manner
- Liaising with Head Office and Suppliers
- Play testing to reproduce any issues customers might have with a particular purchase

I worked through two December / Christmas periods as the Acting Manager as well as multiple game launches.  
I did my best to ensure that the way we worked was efficient.

During this time, I improved our daily and weekly stock take by reducing the time it took from a full day to a couple of hours.  
I also improved the way display racks are sorted; making it easier for customers to find new games in their favorite genres.

[1]: https://www.btgames.co.za/
