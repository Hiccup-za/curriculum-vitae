# Entersekt

<colorBadge text="February 2016 - Present Day" type="green"/>

## Company Background

[Entersekt][1] is a Stellenbosch, South Africa-based fintech provider of mobile-based authentication and app security software;
protecting online and mobile banking and transactions.

## Methodologies

- Agile
- DevOps
- Kanban
- Scrum

## Skills, Stacks & Frameworks

- Automated testing
- Docker
- Gherkin
- Git
- Kubernetes
- Manual testing
- Markdown
- JSON
- REST
- SOAP
- Terraform
- VuePress
- XML
- YAML

## Tools

- AWS
- Argo
- BitBucket
- Confluence
- GitLab
- Insomnia
- Internal automated testing framework
- Internal web-based testing tool
- Jenkins
- Jira
- Portus
- SoapUI
- Zephyr

## Experience

### QA Analyst

<colorBadge text="February 2016 - May 2017" type="blue"/>

I joined the mobile team.

`Product:`

Mobile SDKs and Applications on Android, Blackberry, Feature Phone, iOS and Windows Phone.

`Responsibilities:`

- Creating and executing manual test cases
- Creating and maintaining test plans
- Logging bugs in a clear and concise manner
- Compiling regression results and communicating them to my team
- Presenting a Quality Report in sprint review
- Grooming and estimating testing effort for feature and regression testing

### QE Analyst

<colorBadge text="June 2017 - November 2019" type="blue"/>

During 2017 and 2018 I continued to work with the Mobile team.  
In 2019 I joined the Backend team.

`Product:`

- Mobile SDKs and Applications on Android, Blackberry, Feature Phone, iOS and Windows Phone
- Physical Appliance and software testing

`New Responsibilities:`

- Plan and execute our backend bi-yearly regression and release phases
- Communicate the test results and effort remaining on a daily basis to Product and Engineering

During this time I implemented a Git and Markdown test management and documentation solution using VuePress.

### Senior QE Analyst

<colorBadge text="December 2019 - Present Day" type="blue"/>

As Senior QE Analyst, my focus was moved from a single team to all teams within engineering.

`New Responsibilities:`

- Collaborate with Agile development teams to define and implement product test strategies based on functional and non-functional requirements
- Liaise with developers to create and execute test plans
- Proactively drive quality improvement through metrics and reporting
- Track test coverage across components from code and functional perspectives
- Integrate with multiple Agile development teams partaking in planning, testing and scrum ceremonies
- Advocate quality, best practices and user experience throughout the SDLC
- Ensure that each product adheres to the platform requirements and specifications
- Coordinate and drive the production hardening and release process across multiple teams
- Represent QE and initiate quality related discussions with Product, Dev Managers and Team Leads
- Facilitate and enable the DevOps Core team to groom and plan work on a continuous basis
- Facilitate and enable the Analysts within the QE team to groom and plan work on a continuous basis

[1]: https://www.entersekt.com/
