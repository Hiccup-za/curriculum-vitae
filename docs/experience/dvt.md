# DVT

<colorBadge text="June 2015 – January 2016" type="green"/>

## Company Background

[DVT][1] is a custom software solutions and services partner.

## Skills, Stacks & Frameworks

- Manual Testing
- Markdown
- SOAP
- XML

## Tools

- Bugzilla
- Confluence
- Excel
- Internal web-based testing tool
- Jira
- SoapUI
- Zephyr

## Experience

### Junior SQA Analyst

`Responsibilities:`

- Creating and executing manual test cases daily
- Creating and maintaining test plans
- Logging bugs in a clear and concise manner
- Compiling the results and communicating them to the client daily

#### Client Projects

I worked on a number of client projects:

- Mobile application on Android & iOS
- Power BI project
- Educational game on Windows

#### Entersekt Contractor

<colorBadge text="June 2015 – January 2016" type="blue"/>

I worked directly with the mobile team throughout their feature sprints and tested a number of client releases.

`Products:`

Mobile SDKs and Applications on Android, Blackberry, Feature Phone, iOS and Windows Phone.

`Responsibilities:`

- Creating and executing manual test cases
- Creating and maintaining test plans
- Logging bugs in a clear and concise manner
- Compiling regression results and communicating them to my lead

[1]: https://www.dvt.co.za/
