# Quicksilver

<colorBadge text="September 2012 – November 2012" type="green"/>

## Company Background

[Quicksilver][1] is one of the world's largest brands of surfwear and boardsport-related equipment.

## Experience

### Sales Representative

As a Sales Representative I was responsible for operating the POS system and assisting customers.

My duties also included:

- Ensuring the clothing racks were neat and organized
- Completing stock take with the rest of the team
- Assisting customers with returns and refunds

[1]: https://www.quiksilver.com/
