module.exports = {
  title: 'Curriculum Vitae',
  base: '/curriculum-vitae/',
  dest: './public',
  head: [
    ['link', { rel: 'icon', href: 'images/logo.png' }]
  ],
  theme: 'yuu',
  themeConfig: {
    lastUpdated: 'Last Updated',
    nav: [
      { text: 'Work Experience', link: '/experience/' },
      { text: 'Education', link: '/education/' },
      { text: 'GitLab', link: 'https://gitlab.com/Hiccup-za/curriculum-vitae' }
    ],
    sidebarDepth: 3,
    sidebar: {
      '/education/': [
        '',
      ],
      '/experience/': [
        {
          title: 'Work Experience',
          collapsable: false,
          children: [
            ['', 'Home'],
            ['entersekt', 'Entersekt'],
            ['dvt', 'DVT'],
            ['bt-games', 'BT Games'],
            ['quicksilver', 'Quicksilver'],
            ['mr-video', 'Mr. Video']
          ]
        },
      ],
      '/skill-set/': [
        '',
      ],
    },
    yuu: {
      defaultDarkTheme: true,
      defaultColorTheme: 'blue',
    }
  }
}