# Education

## ISTQB Foundation

<colorBadge text="October 2016" type="green"/>

Completed through `Inspired Testing`.

Credential ID: 2016100069

## Certificate of Competence

<colorBadge text="June 2014" type="green"/>

Completed at `Boston College`.

| Subject               | Percentage Achieved |
| --------------------- | :-----------------: |
| Dreamweaver 1         |         88%         |
| Dreamweaver 2         |         78%         |
| Photoshop 1           |         89%         |
| Photoshop 2           |         82%         |
| Microsoft Access 2010 |         82%         |

## Write a Novel Course

<colorBadge text="September 2013" type="green"/>

Completed through `SA Writers’ College`.

## National Senior Certificate

<colorBadge text="2010" type="green"/>

Matriculated from `Hottentots-Holland High School`.

| Subject                             | Percentage Achieved |
| ----------------------------------- | :-----------------: |
| English Home Language               |         64%         |
| Afrikaans First Additional Language |         62%         |
| Mathematics                         |         52%         |
| Life Orientation                    |         82%         |
| Business Studies                    |         39%         |
| Computer Applications Technology    |         75%         |
| History                             |         51%         |

## 3D Animation Course

<colorBadge text="December 2007" type="green"/>

Completed at the `Andrew Owen School of Art` through evening classes.

Program: Blender
