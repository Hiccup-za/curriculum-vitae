# Changelog

All notable changes to this project will be documented in this file.  
The format is based on [Keep a Changelog][1], and this project adheres to [Semantic Versioning][2].

## [1.2.1] - 2020-08-31

### Updated

- Work experience updates

## [1.2.0] - 2020-08-27

### Added

- My picture to the home page

### Updated

- Home page to contain the About Me and Skill Set content
- Entersekt page
- DVT page

### Removed

- Nav bar links to About Me and Skill Set
- About Me and Skill Set files and folders
- VuePress logo
- About Me button

## [1.1.0] - 2020-08-27

### Added

- About Me content
- Education content
- Skill set content
- Work Experience content

## [1.0.1] - 2020-08-04

### Updated

- Repository README

## [1.0.0] - 2020-08-04

### Added

- VuePress and the Yuu theme
- Stylesheet
- colorBadge component
- Custom container styling
- GitLab CI

[1]: https://keepachangelog.com/en/1.0.0/
[2]: https://semver.org/spec/v2.0.0.html
